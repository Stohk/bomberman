package dev.tilegame.gfx;

import java.awt.image.BufferedImage;

public class Assets {
    //DA QUI CARICHIAMO TUTTI I FILE MULTIMEDIALI
    private static final int playerW = 23, playerH = 24, playerC = 8, playerR = 5,
                             skullW = 46, skullH = 47, skullC = 4, skullR = 2,
                             batW = 52, batH = 48, batC = 5, batR = 2,
                             explosionD = 30, explosionR = 4, explosionC = 4;
    public static BufferedImage[][] black = new BufferedImage[playerR][playerC],
                                    skull = new BufferedImage[skullR][skullC],
                                    bat = new BufferedImage[batR][batC],
                                    explosion = new BufferedImage[explosionR][explosionC];
    public static BufferedImage[] skulldie = new BufferedImage[6],
                                  batdie = new BufferedImage[8],
                                  bomb = new BufferedImage[2],
                                  heart = new BufferedImage[2];
    public static BufferedImage placement, presstostart, menu,writes, chose, win, lose,  map , mattoni, blocco, mattonebru, icon;


    public static void init(){
        int i,z;

        //          Player

        SpriteSheet sheet = new SpriteSheet(ImageLoader.loadImage("/texture/bomber/black.png"));
        for (z=0; z< playerR; z++){
            for (i=0; i< playerC; i++){
                black[z][i] = sheet.crop(i* playerW, z* playerH, playerW, playerH);
            }
        }
        placement = sheet.crop(115, 96, playerW, playerH);
        heart[0] =  ImageLoader.loadImage("/texture/bomber/heart1.png");
        heart[1] =  ImageLoader.loadImage("/texture/bomber/heart2.png");

        //          Skull

        sheet = new SpriteSheet(ImageLoader.loadImage("/texture/mostri/skull.png"));
        for (z=0; z< skullR; z++){
            for (i=0; i< skullC; i++){
                skull[z][i] = sheet.crop(i* skullW, z* skullH, skullW, skullH);
            }
        }
        for (i=0; i< 6; i++){
            skulldie[i] = sheet.crop(i* skullW, 2* skullH, skullW, skullH);
        }

        //          Bat

        sheet = new SpriteSheet(ImageLoader.loadImage("/texture/mostri/bat.png"));
        for (z=0; z< batR; z++){
            for (i=0; i< batC; i++){
                bat[z][i] = sheet.crop(i* batW, z* batH, batW, batH);
            }
        }
        for (i=0; i< 8; i++){
            batdie[i] = sheet.crop(i* batW, 2* batH, batW, batH);
        }

        //          Bomb

        sheet = new SpriteSheet(ImageLoader.loadImage("/texture/bomba/bomb.png"));
        for(i=0;i<2;i++)
            bomb[i] = sheet.crop(i*21, 0, 21, 24);

        //          Explosion

        sheet = new SpriteSheet(ImageLoader.loadImage("/texture/bomba/bombeffect.png"));
        for (z=0; z< explosionR; z++){
            for (i=0; i< explosionC; i++){
                explosion[z][i] = sheet.crop(i* explosionD, z* explosionD, explosionD, explosionD);
            }
        }

        //          General and Map

        presstostart = ImageLoader.loadImage("/texture/transiction/start.png");
        menu = ImageLoader.loadImage("/texture/transiction/menu.png");
        writes = ImageLoader.loadImage("/texture/transiction/writes.png");
        chose = ImageLoader.loadImage("/texture/transiction/chose.png");
        win = ImageLoader.loadImage("/texture/transiction/victory.png");
        lose = ImageLoader.loadImage("/texture/transiction/lose.png");
        map = ImageLoader.loadImage("/texture/mappa/mappa.png");
        mattoni = ImageLoader.loadImage("/texture/mappa/tile.png");
        blocco = ImageLoader.loadImage("/texture/mappa/tile1.png");
        mattonebru = ImageLoader.loadImage("/texture/mappa/tileburned.png");
        icon = ImageLoader.loadImage("/texture/transiction/icona.png");
    }
}
