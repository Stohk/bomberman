package dev.tilegame;

import dev.tilegame.display.Display;
import dev.tilegame.gfx.Assets;
import dev.tilegame.input.KeyManager;
import dev.tilegame.states.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferStrategy;
import javax.swing.Timer;

public class Game implements Runnable {
    private Display display;
    public int width, height;
    public int ticks = 0;
    private final String title;

    private boolean running = false;
    private Thread thread;

    private BufferStrategy bs;
    private Graphics g;

    //States
    private State gameState;
    private State menuState;
    private State startState;
    private State endState;

    //Input
    private KeyManager keyManager;

    public Game(String title, int width, int height){
        this.width = width;
        this.height = height;
        this.title = title;
        keyManager = new KeyManager();
    }

    private void init() {
        //SI INIZIALIZZA SCHERMO E STATES
        display = new Display(title, width, height);
        display.getFrame().addKeyListener(keyManager);


        Assets.init();

        startState = new StartState(this);
        menuState = new MenuState(this);
        gameState = new GameState(this);
        endState = new EndState(this);
        State.setState(startState);
    }

    private void tick() {
        keyManager.tick();
        if(State.getState() != null)
            State.getState().tick();
    }

    private void render() {
        //CHIAMO LO STATE PER DISEGNARE SOPRA IL JFRAME E CANVAS
        bs = display.getCanvas().getBufferStrategy();
        if(bs == null){
            display.getCanvas().createBufferStrategy(3);
            return;
        }
        g = bs.getDrawGraphics();
        //Clear the screen
        g.clearRect(0, 0, width, height);
        //Draw here!

        if(State.getState() != null)
            State.getState().render(g);

        //End drawing!
        bs.show();
        g.dispose();
    }

    public void run() {
        init();
        //FPS => VOLTE IN CUI CHIAMO TICK E RENDER
        int fps = 60;
        double timePerTick = 1000000000 / fps;
        double delta = 0;
        long now;
        long lastTime = System.nanoTime();
        long timer = 0;

        while(running){
            now = System.nanoTime();
            delta += (now - lastTime) / timePerTick;
            timer += now - lastTime;
            lastTime = now;
            //int ticks = 0;

            if (delta>=1){
                tick();
                render();
                ticks ++;
                delta--;
            }

            if(timer>=1000000000){
                //System.out.println("Ticks and Frames: " + ticks);
                ticks = 0;
                timer = 0;
            }
        }

        stop();
    }



    public synchronized void start() {
        if(running)
            return;
        running = true;
        thread = new Thread(this);
        thread.start();
    }

    public synchronized void stop() {
        if(!running)
            return;
        running = false;
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public State getMenuState() {
        return menuState;
    }
    public State getGameState() {
        return gameState;
    }
    public State getEndState() {
        return endState;
    }
    public KeyManager getKeyManager(){
        return keyManager;
    }
}