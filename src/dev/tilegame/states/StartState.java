package dev.tilegame.states;

import dev.tilegame.Game;
import dev.tilegame.gfx.Assets;

import java.awt.Graphics;

public class StartState extends State{

    public static boolean start = false;

    public StartState(Game game) {
        super(game);
    }


    @Override
    public void tick() {
        if(game.getKeyManager().enter){
            start = true;
            State.setState(game.getMenuState());
        }
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(Assets.presstostart, 0, 0, null);
    }
}
