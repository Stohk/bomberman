package dev.tilegame.states;

import dev.tilegame.Game;
import dev.tilegame.display.Display;
import dev.tilegame.gfx.Assets;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MenuState extends State{
    //COME E' FATTO IL MENU

    private int y = 0;

    public MenuState(Game game){
        super(game);
    }

    @Override
    public void tick() {
        if(game.ticks%5 == 0) {
            if (game.getKeyManager().down && (y != 160)) y += 80;
            if (game.getKeyManager().up && (y != 0)) y -= 80;

            //Seleziono lo stato di gioco
            if((y == 0) && (game.getKeyManager().enter) && (!StartState.start)) setState(game.getGameState());


            //Chiudo la finestra di gioco e la virtual machine
            if((y == 160) && (game.getKeyManager().enter)){
                System.exit(0);
            }
            StartState.start = false;
        }
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(Assets.menu, 0 , 0, null);
        g.drawImage(Assets.chose, 10, 215 + y, null);
        g.drawImage(Assets.writes, 40 , 230, null);
    }

}
