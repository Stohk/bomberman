package dev.tilegame.states;

import dev.tilegame.Game;

import java.awt.Graphics;

import static dev.tilegame.gfx.Assets.lose;
import static dev.tilegame.gfx.Assets.win;

public class EndState extends State{

    public static boolean gameover = true;

    public EndState(Game game) {
        super(game);
    }

    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics g) {
        if (gameover)
            g.drawImage(lose, 0 ,0, null);
        else
        g.drawImage(win, 0, 0, null);
    }
}
