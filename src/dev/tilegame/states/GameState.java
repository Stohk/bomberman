package dev.tilegame.states;

import dev.tilegame.Game;
import dev.tilegame.entities.Bomb;
import dev.tilegame.entities.creatures.Bat;
import dev.tilegame.entities.creatures.Skull;
import dev.tilegame.entities.creatures.Player;
import dev.tilegame.entities.Map;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static dev.tilegame.gfx.Assets.heart;

public class GameState extends State{
    //QUI SI GESTISCONO LE MODALITA' DEL GIOCO
    private Player player;
    private Skull skull;
    private Bat bat;
    private Map map;
    private Bomb bomb;

    //Timer
    private int time = 0;
    private int decine=0;
    private int minutes = 0;
    private Font font = new Font("Arial",0,30);

    //Spawn
    private int lifes = 3;
    private int pulse = 0;

    public GameState(Game game){
        super(game);
        t.start();
        map = new Map(0 , 0);
        player = new Player(game, map, 50, 78);
        bomb = new Bomb(game, map, 0,0);
        skull = new Skull(150, 100, map, player);
        bat = new Bat(150, 150, map, player);
    }

    @Override
    public void tick() {
        player.tick();
        map.tick();
        bomb.tick();
        skull.tick();
        bat.tick();

        // Controllo se si è giunti al punto d'arrivo
        if (player.getX() > 530 && player.getY() > 530){
            EndState.gameover = false;
            setState(game.getEndState());
        }

        // Controllo se uno dei mostri ha toccato il player
        if (player.getX() >= skull.getX() && player.getX() <= skull.getX()+15)
            if (player.getY() >= skull.getY() && player.getY() <= skull.getY()+15) {
                lifes--;
                player.setX(50);
                player.setY(78);
                if (lifes == 0){
                    EndState.gameover = true;
                    setState(game.getEndState());
                }
            }
        if (player.getX() >= bat.getX() && player.getX() <= bat.getX()+15)
            if (player.getY() >= bat.getY() && player.getY() <= bat.getY()+15) {
                lifes--;
                if (lifes == 0){
                    EndState.gameover = true;
                    setState(game.getEndState());
                }
            }
        pulse ++;
        if (pulse == 20){
            pulse = 0;
        }
    }

    @Override
    public void render(Graphics g) {
        map.render(g);
        player.render(g);
        skull.render(g);
        bat.render(g);
        bomb.render(g);

        g.setColor(Color.black);
        g.fillRect(0, 0, 600, 30);

        //Timer
        g.setFont(font);
        g.setColor(Color.white);
        g.drawString(""+ minutes + ":" + decine + time,540,26);

        //Vite rimaste
        if (pulse < 10)g.drawImage(heart[0], 285, 3, 30, 30, null);
        if (pulse > 10)g.drawImage(heart[1], 290, 8, 20, 20, null);
        if (lifes>1){
            if (pulse < 10)g.drawImage(heart[0], 250, 3, 30, 30, null);
            if (pulse > 10)g.drawImage(heart[1], 255, 8, 20, 20, null);
        }
        if (lifes>2){
            if (pulse < 10)g.drawImage(heart[0], 320, 3, 30, 30, null);
            if (pulse > 10)g.drawImage(heart[1], 325, 8, 20, 20, null);
        }
    }

    Timer t =new Timer(1000, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            time++;
            if (time == 10){
                decine ++;
                time = 0;
            }
            if (decine == 6){
                minutes ++;
                decine = 0;
            }
        }
    });
}
