package dev.tilegame.entities.creatures;

import dev.tilegame.entities.Bomb;
import dev.tilegame.entities.Map;

import java.awt.Graphics;
import java.util.Random;

import static dev.tilegame.entities.Bomb.espX;
import static dev.tilegame.entities.Bomb.espY;
import static dev.tilegame.gfx.Assets.skull;

public class Skull extends Creature{

    private final int skullW = 26, skullH = 47, RANDOM = 12;
    private final float v = 0.5f;
    private int i = 0, r = 0, n;
    private Map map;
    private Player player;
    private boolean killed = false;

    public Skull(float x, float y, Map map, Player player) {
        super(x, y, 0 ,0);
        this.map = map;
        this.player = player;
    }

    @Override
    public void tick() {
        if(espX == x && espY == y) killed = true;
        if (!killed){

            Random random = new Random();
            i++;
            n = random.nextInt(this.RANDOM);
            n = n%4;
            if ((x - player.getX() < 40 || player.getX() - x < 40) && (y - player.getY() < 40 || player.getY() - y < 40))
                n = 4;
        switch(n){
            case (0):
                do {
                    if (map.checkPosition((int)x+(skullW /2), (int) y+10))y -= v;
                }while (map.checkPosition((int)x+(skullW /2), (int) y+10));
                break;
            case (1):
                do {
                    if (map.checkPosition((int)x+(skullW /2), (int) y+ skullH +(int) v))y += v;
                }while(map.checkPosition((int)x+(skullW /2), (int) y+ skullH +(int)v));
                break;
            case (2):
                do {
                    if (map.checkPosition((int)x+ skullW +(int) v, (int) y+(skullH /2)))x += v;
                }while (map.checkPosition((int)x+ skullW +(int) v, (int) y+(skullH /2)));
                break;
            case (3):
                do {
                    if (map.checkPosition((int)x-(int) v, (int) y+(skullH /2)))x -= v;
                }while (map.checkPosition((int)x-(int) v, (int) y+(skullH /2)));
                break;
            case (4):
                if(map.checkPosition((int)x-(int) v, (int) y+(skullH /2)))
                    if (x - player.getX() > 0) x-= v;
                if(map.checkPosition((int)x+ skullW +(int) v, (int) y+(skullH /2)))
                    if (x - player.getX() < 0) x+= v;
                if(map.checkPosition((int)x+(skullW /2), (int) y+10))
                    if (y - player.getY() > 0) y -= v;
                if(map.checkPosition((int)x+(skullW /2), (int) y+ skullH +(int) v))
                    if (y - player.getY() < 0) y += v;
                break;
        }
            if(i==4) i = 0;
        }
    }

    @Override
    public void render(Graphics g) {
        if (!killed) {
            g.drawImage(skull[r][i], (int) x, (int) y, null);
        }
    }
}
