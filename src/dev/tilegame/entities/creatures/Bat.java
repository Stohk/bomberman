package dev.tilegame.entities.creatures;

import dev.tilegame.entities.Map;

import java.awt.Graphics;
import java.util.Random;

import static dev.tilegame.gfx.Assets.bat;

public class Bat extends Creature{

    private final int batW = 36, batH = 45, RANDOM = 12;
    private final float v = 0.5f;
    private int i = 0, r = 0, n;
    private Map map;
    private Player player;

    public Bat(float x, float y, Map map, Player player) {
        super(x, y, 0 ,0);
        this.map = map;
        this.player = player;
    }

    @Override
    public void tick() {
        Random random = new Random();
        i++;
        n = random.nextInt(this.RANDOM);
        n = n%4;
        if ((x - player.getX() < 40 || player.getX() - x < 40) && (y - player.getY() < 40 || player.getY() - y < 40))
            n = 4;
        switch(n){
            case (0):
                if(map.checkPosition((int)x+(batW /2), (int) y+10))y -= v;
                break;
            case (1):
                if(map.checkPosition((int)x+(batW /2), (int) y+ batH +(int) v))y += v;
                break;
            case (2):
                if(map.checkPosition((int)x+ batW +(int) v, (int) y+(batH /2)))x += v;
                break;
            case (3):
                if(map.checkPosition((int)x-(int) v, (int) y+(batH /2)))x -= v;
                break;
            case (4):
                if(map.checkPosition((int)x-(int) v, (int) y+(batH /2)))
                    if (x - player.getX() > 0) x-= v;
                if(map.checkPosition((int)x+ batW +(int) v, (int) y+(batH /2)))
                    if (x - player.getX() < 0) x+= v;
                if(map.checkPosition((int)x+(batW /2), (int) y+10))
                    if (y - player.getY() > 0) y -= v;
                if(map.checkPosition((int)x+(batW /2), (int) y+ batH +(int) v))
                    if (y - player.getY() < 0) y += v;
            break;
        }
        if(i==5) i = 0;
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(bat[r][i], (int) x, (int) y,null);
    }
}
