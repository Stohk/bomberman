package dev.tilegame.entities.creatures;

import dev.tilegame.Game;
import dev.tilegame.entities.Bomb;
import dev.tilegame.entities.Map;
import dev.tilegame.states.EndState;
import dev.tilegame.states.State;

import java.awt.Graphics;

import static dev.tilegame.gfx.Assets.black;
import static dev.tilegame.gfx.Assets.placement;

public class Player extends Creature {

    private Game game;
    private Map map;

    private final int DOWN = 0, RIGHT = 1, UP = 2, LEFT = 3, DIE =4;
    private int pos = DOWN, i = 0, c = 0;
    private boolean placing = false;

    public Player(Game game, Map map, float x, float y) {
        super(x, y, Creature.DEFAULT_CREATURE_WIDTH, Creature.DEFAULT_CREATURE_HEIGHT);
        this.game = game;
        this.map = map;
    }

    @Override
    public void tick() {
        getWASD();
        move();

        //PIAZZAMENTO

        if(game.getKeyManager().space && !Bomb.placed && !Bomb.exploded){
            Bomb.getXY (x + width/2, y + height/2);
            c = 0;
            placing = true;
        }
        if (placing){
            c++;
            if (c == 15){
                c = 0;
                placing = false;
            }
        }
    }

    public void getWASD() {
        xMove = 0;
        yMove = 0;

        //MOVIMENTO

        if(game.getKeyManager().up) {
            if(map.checkPosition((int)x+(width/2), (int) y+10))
                yMove = -speed;
            i++;
            if(i==8) i=0;
            pos = UP;
        }
        if(game.getKeyManager().down) {
            if(map.checkPosition((int)x+(width/2), (int) y+height+(int)speed))
                yMove = speed;
            i++;
            if(i==8) i=0;
            pos = DOWN;
        }
        if(game.getKeyManager().left) {
            if(map.checkPosition((int)x, (int) y+(height/2)))
                xMove = - speed;
            i++;
            if(i==8) i=0;
            pos = LEFT;
        }
        if(game.getKeyManager().right) {
            if(map.checkPosition((int)x+width, (int) y+(height/2)))
                xMove = speed;
            i++;
            if(i==8) i=0;
            pos = RIGHT;
        }
    }

    @Override
    public void render(Graphics g) {
        if (placing) g.drawImage(placement, (int) x, (int) y, null);
        else g.drawImage(black[pos][i], (int) x, (int) y, width, height, null);
    }
}
