package dev.tilegame.entities;

import dev.tilegame.Game;
import dev.tilegame.entities.creatures.Player;
import dev.tilegame.gfx.Assets;

import java.awt.Graphics;

public class Bomb extends Entity{

    private Game game;
    private Map map;

    private int c, i, z;
    public static int xb, yb, espX, espY;
    public static boolean placed = false,
                          exploded = false,
                          wall = false;

    public Bomb(Game game, Map map, float x, float y) {
        super(x, y, 6, 6);
        this.game = game;
        this.map = map;
    }

    //      Prendo la posizione dal giocatore

    public static void getXY(float x, float y){
        xb = (int) (x - x%30);
        yb = (int) (y - y%30);
    }

    @Override
    public void tick() {
        if ((game.getKeyManager().space) && (!placed) && (!exploded)){
            placed = true;
            c = 0;
        }
        if (placed) {
            c++;
            z++;
            if (z == 20) z = 0;
            if (c == 60) {
                c = 0;
                placed = false;
                exploded = true;
            }
        }
        if (exploded) {
            c++;
            if (c == 40) {
                c = 0;
                exploded = false;
            }
        }
    }

    @Override
    public void render(Graphics g) {
        if (placed)
            g.drawImage(Assets.bomb[z/10], xb + 8, yb + 1, null);
        if (exploded) {
            g.drawImage(Assets.explosion[0][c/10], xb, yb, null);
            //SOPRA
            wall = false;
            i = 1;
            do{
                espX = xb;
                espY = yb - i * 30;
                if (map.checkPosition(espX, espY)) {
                    g.drawImage(Assets.explosion[3][c / 10], espX, espY, null);
                    i++;
                }
                else {
                    wall = true;
                    if(c == 1)map.changeTile(espX, espY);
                    if(c == 39)map.eraseTile(espX, espY);
                }
            }while(!wall);
            //SOTTO
            wall = false;
            i = 1;
            do{
                espX = xb;
                espY = yb + i * 30;
                if (map.checkPosition(espX, espY)) {
                    g.drawImage(Assets.explosion[3][c / 10], espX, espY, null);
                    i++;
                }
                else {
                    wall = true;
                    if(c == 1)map.changeTile(espX, espY);
                    if(c == 39)map.eraseTile(espX, espY);
                }
            }while(!wall);
            //DESTRA
            wall = false;
            i = 1;
            do{
                espX = xb + i * 30;
                espY = yb;
                if (map.checkPosition(espX, espY)) {
                    g.drawImage(Assets.explosion[1][c / 10], espX, espY, null);
                    i ++;
                }
                else {
                    wall = true;
                    if(c == 1)map.changeTile(espX, espY);
                    if(c == 39)map.eraseTile(espX, espY);
                }
            }while(!wall);
            //SINISTRA
            wall = false;
            i = 1;
            do{
                espX = xb - i * 30;
                espY = yb;
                if (map.checkPosition(espX, espY)) {
                    g.drawImage(Assets.explosion[1][c / 10], espX, espY, null);
                    i++;
                }
                else {
                    wall = true;
                    if(c == 1)map.changeTile(espX, espY);
                    if(c == 39)map.eraseTile(espX, espY);
                }
            }while(!wall);
        }
    }

    public static int getXb() {
        return xb;
    }

    public static int getYb() {
        return yb;
    }
}
